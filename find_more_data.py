import cv2
from detectFace import *
import os
import copy
from sklearn.metrics.pairwise import cosine_similarity

import caffe

caffe.set_device(0)
caffe.set_mode_gpu()
net = caffe.Net('model/sphereface_deploy.prototxt','model/sphereface_model.caffemodel',
                caffe.TEST)

def calculate_vector(image):
    img = cv2.imread(image)
    # print(image)
    # cv2.imshow('org '+name, img)
    # img = extract_face_crop(img)

    img = img -127.5
    img = img * 1.0 / 128
    img = np.transpose(img, (2,0,1))
    img = np.expand_dims(img, 0)
    net.blobs['data'].data[...] = img
    out1 = net.forward()
    value1 = copy.deepcopy(out1)
    return value1['fc5'][0]

emb_not_tag = []
emb_other_person = []
emb_own_person = []


# def calculator_cosine(list_1, list_2):
#     """
#     Calculator cosine each pairs of 2 list
#     list_1, list_2: 2 list of array 512-d
#     """
#     norm_1 = np.linalg.norm(list_1, axis=1)
#     print('norm 1', norm_1)
#     norm_2 = np.linalg.norm(list_2, axis=1)
#     print('norm 2', norm_2)
#     norm_1 = np.expand_dims(norm_1, axis=1)


#     list_1 = np.expand_dims(list_1, axis=1)
#     dot_product = np.sum(list_1 * list_2, axis=2)
#     print('dot product ', dot_product)

#     norm_mul = np.array(norm_1 * norm_2)
#     print('Norm, mul ',norm_mul)
#     print(dot_product/ norm_mul)

def calculator_cosine(list_1, list_2):
    """
    Calculator cosine each pairs of 2 list
    list_1, list_2: 2 list of array 512-d
    """

    norm_1 = np.linalg.norm(list_1, axis=1, keepdims=True)
    norm_2 = np.linalg.norm(list_2, axis=1, keepdims=True)
    normalise_1 = list_1 / norm_1
    normalise_2 = list_2 / norm_2
    return np.dot(normalise_1, normalise_2.T)


def cosine_2_vector(v1, v2):
    v1 = np.array(v1)
    v2 = np.array(v2)
    dot_product = np.sum(v1 * v2)
    return(dot_product / (np.linalg.norm(v1) * np.linalg.norm(v2)))


def filter_data_id(folder_id):
    """
    clear noise of dataset using sphere
    folder_id : str
        inclue  3 folder "not_tag", "other_person", "own_person"

    delete images in folder "not_tag" which is same person with person in other_person  in case own_person haven't images
    """
    emb_not_tag = []
    emb_other_person = []
    emb_own_person = []
    link_image = []
    # print('process not tag')
    # for img_file in os.listdir(folder_id + '/not_tag'):
    #     emb_not_tag.append(calculate_vector(folder_id + '/not_tag/' + img_file))
    # print('other person')
    # for img_file in os.listdir(folder_id + '/other_person'):
    #     emb_other_person.append(calculate_vector(folder_id + '/other_person/' + img_file))
    print('own person')
    for img_file in os.listdir(folder_id + '/own_person'):
        emb_own_person.append(calculate_vector(folder_id + '/own_person/' + img_file))
        link_image.append(folder_id + '/own_person/' + img_file)

    # if len(emb_own_person) == 0:
    # t1 = 10
    # t2 = 30
    # emb_own_person[t1] = np.expand_dims(emb_own_person[t1], axis=0)
    # emb_own_person[t2] = np.expand_dims(emb_own_person[t2], axis=0)
    # print(emb_own_person[20])
    # print(emb_own_person[30])
    cosine_matrix = calculator_cosine(emb_own_person, emb_own_person)

    a, b = np.where(cosine_matrix < 0.5)
    print(len(emb_own_person) ** 2)
    print(a.shape[0])
    for i in range(a.shape[0]):
        cv2.imshow('d', cv2.imread(link_image[a[i]]))
        cv2.imshow('d2', cv2.imread(link_image[b[i]]))
        cv2.waitKey(0)
    # cv2.imshow('d', cv2.imread(link_image[t1]))
    # cv2.imshow('d1', cv2.imread(link_image[t2]))
    # cv2.waitKey(0)

print('zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz')
filter_data_id('upload_save/1598642598')
