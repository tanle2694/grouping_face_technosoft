

from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler
import numpy as np
import shutil
import pickle
import os
import cv2
from detectFace import *
import os
import copy
from sklearn.metrics.pairwise import cosine_similarity

import caffe

caffe.set_device(0)
caffe.set_mode_gpu()
net = caffe.Net('model/sphereface_deploy.prototxt','model/sphereface_model.caffemodel',
                caffe.TEST)

def calculate_vector(image):
    img = cv2.imread(image)
    # print(image)
    # cv2.imshow('org '+name, img)
    # img = extract_face_crop(img)

    img = img -127.5
    img = img * 1.0 / 128
    img = np.transpose(img, (2,0,1))
    img = np.expand_dims(img, 0)
    net.blobs['data'].data[...] = img
    out1 = net.forward()
    value1 = copy.deepcopy(out1)
    return value1['fc5'][0]

def grouping_id(folder_id, folder_save):
    # os.mkdir(folder_save + '/' + id)
    # os.mkdir(file_id_unique + '/' + id)
    # file = open('data/emb_output/' + id + '/emb.pkl', 'rb')
    # link_images = pickle.load(file)
    # emb = pickle.load(file)
    # file.close()
    link_images = []
    emb = []
    for file in os.listdir(folder_id):
        link_images.append(folder_id + '/' + file)
        emb.append(calculate_vector(folder_id + '/' + file))

    emb = np.array(emb)
    if emb.shape[0] == 0:
         return 0
    X = StandardScaler().fit_transform(emb)
    db = DBSCAN(eps=0.5, min_samples=5, metric='cosine').fit(X)

    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True

    labels = db.labels_
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    link_list_image = []
    for i in range(n_clusters_):
        os.makedirs(folder_save + '/' + str(i))
        link_list_image.append([])

    for (i, cluster) in enumerate(labels):
        if cluster != -1:
            link_list_image[cluster].append(link_images[i])

    for (i, l) in enumerate(link_list_image):
        for k in l:
            split_name = k.split('/')
            shutil.copy(k, folder_save + '/' + str(i)+'/' + split_name[-1])

    # max_number_image = -1
    # index_id_has_max_number = -1
    # for(i, l) in enumerate(link_list_image):
    #         if len(l) > max_number_image:
    #             index_id_has_max_number = i
    #             max_number_image = len(l)
    #             # print(i)

    # if max_number_image != -1:
    #     for i in link_list_image[index_id_has_max_number]:
    #         # print(i)
    #         split_name = i.split('/')
    #         shutil.copy(i, file_id_unique + '/' + id + '/' + split_name[-1])

folder_id = 'upload_save/100000245050350/not_tag'
folder_save = 'group'
grouping_id(folder_id, folder_save=folder_save)
